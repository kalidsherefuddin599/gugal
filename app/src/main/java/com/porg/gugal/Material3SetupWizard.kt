/*
 *     Material3SetupWizard.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp

class Material3SetupWizard {

    companion object {
        @Composable
        fun Header(text: String, doFinish: () -> Unit) {
            IconButton(
                onClick =doFinish,
                modifier = Modifier.
                then(Modifier.padding(start = 16.dp, top = 16.dp, bottom = 0.dp, end = 16.dp))
            ) {
                Icon(
                    imageVector = Icons.Filled.ArrowBack,
                    contentDescription = "Go back",
                )
            }
            Text(
                text = text,
                modifier = Modifier
                    .padding(start = 24.dp, top = 72.dp, bottom = 24.dp, end = 24.dp)
                    .fillMaxWidth(),
                style = MaterialTheme.typography.displaySmall
            )
        }

        @Composable
        fun TwoButtons(positive: () -> Unit, positiveText: String, negative: () -> Unit, negativeText: String = "Back") {
            Box(
                modifier = Modifier.fillMaxSize().then(PaddingModifier),
                Alignment.BottomCenter
            ) {
                Surface(modifier = Modifier.fillMaxWidth(),color = MaterialTheme.colorScheme.background, tonalElevation = 0.dp) {
                    Box (modifier = Modifier.fillMaxWidth()) {
                        Button(
                            modifier = Modifier.padding(all = 4.dp).align(Alignment.BottomEnd),
                            onClick = positive
                        ) {
                            Text(positiveText)
                        }
                        if (negative != null) {
                            OutlinedButton(
                                modifier = Modifier.padding(all = 4.dp).align(Alignment.BottomStart),
                                onClick = negative
                            ) {
                                Text(negativeText)
                            }
                        }
                    }
                }
            }
        }

        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        fun Tip(text: String, modifier: Modifier, image: Int, onClick: (() -> Unit)?) {
            if (onClick != null) {
                Surface(
                    modifier = modifier,
                    shape = RoundedCornerShape(20.dp),
                    tonalElevation = 2.dp,
                    onClick = onClick
                ) {TipContent(text, image)}
            } else {
                Surface(
                    modifier = modifier,
                    shape = RoundedCornerShape(20.dp),
                    tonalElevation = 2.dp
                ) {TipContent(text, image)}
            }
        }

        @Composable
        private fun TipContent(text: String, image: Int) {
            Row(
                modifier = Modifier.padding(all = 14.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painterResource(image),
                    modifier = Modifier.size(36.dp),
                    colorFilter = ColorFilter.tint(
                        MaterialTheme.colorScheme.secondary
                    ),
                    contentDescription = ""
                )
                Text(
                    text = text,
                    modifier = Modifier.padding(start = 14.dp),
                    style = MaterialTheme.typography.bodyMedium
                )
            }
        }

        @Composable
        fun Tip(text: String, modifier: Modifier, image: Int) {
            Tip(text, modifier, image, null)
        }

        val PaddingModifier: Modifier = Modifier.padding(16.dp)
    }
}