/*
 *     NewsActivity.kt
 *     Gugal
 *     Copyright (c) 2022 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.porg.gugal.ui.theme.GugalTheme

@ExperimentalMaterialApi
class NewsActivity : ComponentActivity() {

    private var apikey = ""
    private var cx = ""

    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GugalTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                }
            }
        }
    }


    @Composable
    fun ArticleCard(res: Article) {
        Surface(
            shape = MaterialTheme.shapes.medium,
            elevation = 1.dp,
            modifier = Modifier.padding(all = 4.dp),
            onClick = {
                if (res.url != "") {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(res.url))
                    // Note the Chooser below. If no applications match,
                    // Android displays a system message.So here there is no need for try-catch.
                    startActivity(Intent.createChooser(intent, "Open result in"))
                }
            }
        ) {
            Column {
                Text(
                    text = res.title,
                    modifier = Modifier.padding(all = 4.dp),
                    style = MaterialTheme.typography.h6
                )
                // Add a vertical space between the author and message texts
                Spacer(modifier = Modifier.height(2.dp))
                Text(
                    text = res.body,
                    modifier = Modifier.padding(all = 4.dp),
                    style = MaterialTheme.typography.body2
                )
                Spacer(modifier = Modifier.height(2.dp))
                // Make second text
                var txt: String = ""
                if (res.site != "")   txt = res.site
                if (res.author != "") txt = "$txt - by ${res.author}"
                if (txt != "") {
                    Text(
                        text = txt,
                        modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, start = 4.dp, end = 4.dp),
                        style = MaterialTheme.typography.caption
                    )
                }
            }
        }
    }

    @Composable
    @Preview
    fun PreviewArticleCard() {
        GugalTheme {
            ArticleCard(
                res = Article(
                    "Google privacy scandal",
                    "Google have been fined for antitrust once again, a few days after people bought more than 5 Pixels.",
                    "about:blank",
                    "Test",
                    "TestNews")
            )
        }
    }

}