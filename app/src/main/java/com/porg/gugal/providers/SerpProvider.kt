/*
 *     SerpProvider.kt
 *     Gugal
 *     Copyright (c) 2021 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers

import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import com.android.volley.toolbox.JsonObjectRequest
import com.porg.gugal.Result

/**
 * Allows Gugal to search the web using a search engine.
 */
interface SerpProvider {

    /**
     * A Composable which provides options to set up the SERP provider
     * (e.g. input fields for any required API keys or username and password fields).
     *
     * Shown in the *Set up search* page of the setup wizard.
     *
     * @param modifier provided by the app and should be applied to the composable's root view
     * (e.g. using [Modifier.then()]).
     */
    @Composable fun ConfigComposable(modifier: Modifier) {}

    /**
     * Perform a web search for the provided query and add results to the provided list.
     *
     * @param query the query to search for.
     * @param resultList a list of results that will be shown in the app.
     * @return a [JsonObjectRequest] which will be executed by the app, or `null` if the provider doesn't use the Volley library or JSON.
     */
    fun search(query: String, resultList: SnapshotStateList<Result>): JsonObjectRequest? {return null}

    /**
     * Get any sensitive credentials, e.g. API keys and passwords.
     */
    fun getSensitiveCredentials(): Map<String, String> {
        return mapOf("key" to "value")
    }

    /**
     * Gets called by the app after it loads the user's sensitive credentials (e.g. API keys and passwords) for this provider.
     *
     * @param credentials the loaded sensitive credentials.
     */
    fun useSensitiveCredentials(credentials: Map<String, String>) {}

    companion object

    /**
     * A unique ID that identifies this provider.
     */
    val Companion.id: String get() = ""
    /**
     * A unique ID that identifies this provider.
     */
    val id: String get() = Companion.id

    /**
     * Information about this provider, like the name and description.
     */
    val providerInfo: ProviderInfo?
}