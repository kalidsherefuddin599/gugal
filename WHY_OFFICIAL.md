# Only download Gugal from official sources!

Unofficial versions of Gugal can contain malware, spyware or adware.

Also, Gugal saves credentials which are used to search Google. An unofficial, malicious version of Gugal might steal those credentials and potentially cost you money, as the Custom Search Engine API is only free for 100 queries per day.

## Which sources are official?

The only official sources are [the GitLab releases page](https://gitlab.com/narektor/gugal/-/releases) and F-Droid.
